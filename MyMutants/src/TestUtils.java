import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import caseine.tags.ToDo;
import caseine.tags.ToDoIn;

public class TestUtils {

	/**
	 * Votre premier test.
	 */
	@Test
	public void testPositive() {
		assertEquals(true, Utils.isPositive(10));
	}

	/**
	 * Test caché à l'étudiant.
	 */
	@Test
	@ToDo
	public void testPositiveWithNegativeInput() {
		assertEquals(false, Utils.isPositive(-5));
	}

	/**
	 * Test caché avec un indice.
	 */
	@Test
	@ToDoIn("N'oublie pas de tester les cas limites!")
	public void testPositiveWithZero() {
		assertEquals(true, Utils.isPositive(0));
	}

}
