public class Utils {

	private Utils() {
	}

	public static boolean isPositive(int number) {

		boolean result = false;
		if (number >= 0) {
			result = true;
		}
		return result;
	}
}
