package mutant_01;

import caseine.extra.utils.mutations.Mutant;

@Mutant(testClass = "TestUtils", errorMessage = "Test positive input")
public class Utils {

	private Utils() {
	}

	public static boolean isPositive(int number) {

		boolean result = true;
		if (number < 0 || number > 0) {
			result = false;
		}

		return result;
	}
}
