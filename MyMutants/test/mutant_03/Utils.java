package mutant_03;

import caseine.extra.utils.mutations.Mutant;

@Mutant(testClass = "TestUtils", errorMessage = "Test limits")
public class Utils {

	private Utils() {
	}

	public static boolean isPositive(int number) {

		boolean result = false;
		if (number > 0) {
			result = true;
		}
		if (number < 0) {
			result = false;
		}
		return result;
	}
}
